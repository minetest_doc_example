# Example mod for the Help modpack (`doc`, `doc_items`, etc.)
This mod is for modders who want to learn how to use the APIs of the mods
`doc`, `doc_items` and `doc_identifier` from the Help modpack.
It is useless for anyone else. This mod should only be used for testing
and learning. not for actual playing or for servers, as it will only clutter
the the help categories.

It demonstrates the usage of the APIs of the Help mods. This should help you
in getting started with this modpack.

This mod includes examples for the following mods:

* `doc_items`: In `doc_items.lua`.
         For adding item help (including nodes). Start here!
* `doc_identifier`: In `doc_identifier.lua`.
         This is for adding lookup tool support for custom entities.
* `doc`: In `doc.lua`, for adding your own categories and entries.
         This is rather heavy stuff, intended for advanced modding.
         But is also very flexible.

This example mod only shows the most frequently-needed features to help
you get started. For most use cases, this knowledge might already
suffice. You do not need to understand everything to use this modpack.
Read `API.md` of the respective mods to learn how to use the Help mods
properly.

Using the APIs of the Help modpack helps to improve the overall quality
and completeness of the entire help.

Activate this mod in your world to see some example categories, entries and
items added.

Read the source code files in this mod for some brief explanations.
