-- Example for using doc_identifier (Lookup Tool)
-- Reminder: Use the API to make custom entities identifiable by the lookup tool.
-- If you do not define any custom entities, you don't need this API.
-- This example adds one example entity and adds doc_identifier support for it.

-- To spawn this entity, use the chat command “/spawnentity doc_example:entity”
minetest.register_entity("doc_example:entity", {
	physical = true,
	collide_with_objects = true,
	visual = "cube",
	size = { x = 0.2, y = 0.2, z = 0.2 },
	textures = {
		"unknown_object.png", "unknown_object.png",
		"unknown_object.png", "unknown_object.png",
		"unknown_object.png", "unknown_object.png"
	},
	hp_max = 1,
})

-- This is the actual code you need to add doc_identifier support.
doc.sub.identifier.register_object("doc_example:entity", "example1", "entity")

--[[ Yes, this is all you need!
The code means, whenever you use the lookup tool on the example
entity, the entry “entity” in category “example1” will be shown. ]]

--[[ NOTE: If you don't do this, the lookup tool will show an error message
when you use it on this entity. ]]
