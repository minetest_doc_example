--[[ This is the doc_items example.
This example shows how to add help texts to items and how to use
other frequently-used features of doc_items.
]]

--[[ IMPORTANT REMINDER: doc_items mod will automatically generate help
entries for all items (a few exceptions apply) without your intervention.
doc_items already extracts a lot of item definition automatically.
The API is mainly concerned about enhancing the existing item entries.

Hint: When using this example, use the `/help_reveal`
chat command to reveal all item entries (for testing) ]]

--[[ HELP TEXTS ]]

-- For nontrivial items, you typically want to use the _doc_items_* fields
minetest.register_craftitem("doc_example:item1", {
	description = "doc_example test item 1",
	-- This is the typical way to add extended item descriptions.
	_doc_items_longdesc = "This is an useless example item. It does nothing.",
	--[[ This simple item is self-explanatory, so we can omit
	-- _doc_items_usagehelp.
	For more fields, see API.md of doc_items. ]]

	-- Just an example group
	group = { example = 1 },
})

-- These are just more example items which we use for the factoids later
minetest.register_craftitem("doc_example:item2", {
	description = "doc_example test item 2",
	group = { example = 2 },
})

minetest.register_craftitem("doc_example:item3", {
	description = "doc_example test item 3",
	group = { example = 25 },
})

minetest.register_tool("doc_example:tool", {
	description = "doc_example chat tool",
	_doc_items_longdesc = "This tool is able to write something into the chat.",
	--[[ This tool has an unique non-standard use (i.e. not mining, not melee combat,
	etc.), so we should add this field as well
	Read API.md of doc_items for guidelines to write good help texts ]]
	_doc_items_usagehelp = "Punch to send a chat message.",
	-- The tool entry will be visible for everyone at start
	_doc_items_hidden = false,
	on_punch = function()
		minetest.chat_send_all("The doc_example chat tool has been used!")
	end,
})

--[[ FACTOIDS
	Reminder: A factoid is an automatically generated text based on the item
	definition. This section will demonstrate the use of factoids.
]]

--[[ This adds an automatically generated text for all items which are a member
of the example group. ]]
doc.sub.items.register_factoid("craftitems", "groups", function(category_id, def)
	if def.groups.example then
		return string.format("Example factoid: This item is member of the example group at rating %d.",
				def.groups.example)
	else
		--[[ It is legal to return an empty string. In that case, nothing,
		not even an empty line, will be added. ]]
		return ""
	end
end)

-- This factoid adds the drawtype for nodes
doc.sub.items.register_factoid("nodes", "misc", function(category_id, def)
	return string.format("Example factoid: This item has the drawtype “%s”.",
			def.drawtype)
end)


--[[ USING ENTRY ALIASES
Sometimes, mods register multiple items for basically the same thing in
different “states”. One example is the furnace from Minetest Game
(default:furnace and default:furnace_active).
If the node in all “states” is similar enough, you might consider to
suppress the entries of some of the entries in order to concentrate
information on a single entry. If you do this, you also have to register
aliases since the lookup tool will stop working on nodes with suppressed
entries. Suppressing entries may also be useful to combine large lists
of similar items, but be careful not to overdo it and throw away
potentially valuable information in the process.

The following code shows two nodes which changes its textures when punched.
For the player, doc_example:node2 is basically the same as doc_example:node1,
only with a different texture. Only one entry is created for both nodes.
]]
-- Node for the first “state”. This no
minetest.register_node("doc_example:node1", {
	description = "doc_example test node",
	_doc_items_longdesc = "When rightclicked, the textures of this block change.",
	tiles = {"doc_example_1.png"},
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		minetest.set_node(pos, {name="doc_example:node2"})
	end,
	groups = {dig_immediate=2},
})

-- Node for the second “state”. No entry will be created for this.
minetest.register_node("doc_example:node2", {
	description = "doc_example test node",
	-- Suppresses the entry
	_doc_items_create_entry = false,
	--[[ Because the entry is suppressed, we do not need to add other _doc_items_*
	fields; they would be ignored anyway. ]]
	tiles = {"doc_example_2.png"},
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		minetest.set_node(pos, {name="doc_example:node1"})
	end,
	groups = {dig_immediate=2, not_in_creative_inventory=1},
	drop = "doc_example:node1",
})

--[[ Register an alias to make sure the lookup tool still works.
If this is NOT done, the lookup tool will fail when it is
used on doc_example:node2. ]]
doc.add_entry_alias("nodes", "doc_example:node1", "nodes", "doc_example:node2")

-- Another example of this concept in action be seen in the mod “doc_identifier”.
